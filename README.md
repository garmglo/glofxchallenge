# Trading Global Income Coin Challenge
This tiny repository is intended to help you on your way with the Global Income Coin challenge. The goal of this challenge is to create a script that executes buys of the "GLO" token in order to increase the exchange rate of GLO to its target price of $1 USD.

As the GLO token doesn't exist yet, however, we are going to simulate this by trading against a well-known token: TRX. For the purposes of this exercise, whatever the exchange rate is the first time it is read, it should always be 10% below the target exchange rate so the script starts trading against it.

Here are the high level requirements that this script should implement:
1. Read the current trading price of TRX expressed in USDT
2. Establish that this trading price is 10% under the target exchange rate
3. Sets a clear target to influence the exchange rate 10% upward
4. Gradually make trades to increase the exchange rate
5. Spends at most 1% of the treasury reserves every 24 hours to influence the exchange rate


## To get started working on this challenge this challenge
1. Install the python-binance package by running `pip install -r requirements.txt`
2. Visit https://testnet.binance.vision/ to generate API keys for binance's test environment
3. Set these API keys in glofx.py
4. Run glofx.py with `python glofx.py` to validate everything is working correctly
5. Start coding :-)
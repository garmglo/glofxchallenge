from binance import Client, ThreadedWebsocketManager, ThreadedDepthCacheManager

api_key = "" #replace me with key from https://testnet.binance.vision/
api_secret = "" #replace me with key from https://testnet.binance.vision/
client = Client(api_key, api_secret, testnet=True)

def main():
  grow_usdt_treasury(client) #only run this the first time to sell the 1 BTC you get with your binance api key, then comment out
  usdt_treasury = client.get_asset_balance(asset='USDT')
  account = client.get_account()
  print("currently in treasury: ", usdt_treasury['free'])

def grow_usdt_treasury(client):
  print("selling 1 bitcoin for usdt to grow the USDT treasury")
  return client.create_order(
    symbol='BTCUSDT',
    side=Client.SIDE_SELL,
    type=Client.ORDER_TYPE_MARKET,
    quantity=1)


def glo_price(client):
  return client.get_avg_price(symbol="TRXUSDT")


if __name__ == '__main__':
  main()